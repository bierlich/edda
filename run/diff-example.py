from edda import Backup
from datetime import datetime, timedelta

# Backup method.
method = 'diff'

# Location(s) to be backed up.
sources = ['/home/lovecraft/']

# Target folder for differential backup.
target = '/scratch/galette/bierlich/backup-test/'

# Exclude these subfolders of sources.
excludes = ['tmp', 'README.txt', 'lost+found']

# Don't back up directories with these names.
nobackup = ['Downloads', 'Dropbox', '.dropbox', '.thunderbird', '.mozilla', '.cache']

# Logfile name (verbose)
logfile = 'log.txt'

# Target folder for zipped snapshots.
stash = '/scratch/galette/bierlich/backup-test/stash/'

# Number of threads for bzipping snapshots.
nt = 6

# The backup object.
b = Backup('daily', method, logfile, sources, target, stash, excludes, nobackup)

try:
    b.backup()
    now = datetime.now()
    s = b.name+'-'+now.strftime("%Y-%m-%d_%H:%M")
    b.stash(s, nt)
    b.finish('Backup finished succesfully.')

except Exception as ex:
    print(ex)
    b.finish('Backup finished with error.')
