from edda import Backup, Alert, Logger
from datetime import datetime, timedelta

# Name of this backup instance.
name = 'test'

# Backup method.
method = 'snap'

# Location(s) to be backed up.
sources = ['/home/bierlich/backsource/', '/home/bierlich/backsource2']

# Target folder for differential backup.
target = '/home/bierlich/backtest/snap/'

# Exclude these subfolders of sources.
#excludes = ['tmp', 'README.txt', 'lost+found']
excludes = []
# Don't back up directories with these names.
#nobackup = ['Downloads','.thunderbird','.mozilla','.cache']
nobackup = []
# Logfile name (verbose)
logfile = 'log-test.txt'

# Target folder for snapshots.
stash = '/home/bierlich/backtest/stash-test/'

# Number of threads for bzipping snapshots.
nt = 1

# The logging object.
logger = Logger(name, logfile)

# The backup object.
b = Backup(name, logger, method, sources, target, stash, excludes, nobackup)

# Mail alerts object.
a = Alert(name, logger)

try:
    b.backup()
    b.finish('Backup finished succesfully.')
    a.sendCompletedNoErrors()

except Exception as ex:
    print(ex)
    b.finish('Backup finished with error.')
    a.sendCompletedWithErrors()
