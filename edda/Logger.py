# -*- coding: utf-8 -*-
from datetime import datetime
import subprocess
import shutil

# Logging class for having a persistent logger object
# across processes.

class Logger():
    def __init__(self, name, logfile):
        self.name = name
        self.logfile = logfile
        self.nline = 0
        self.printToLog('\n=========================',False,'a+')
        self.printToLog('New logged session.')

    # Print to the log file with optional time stamp.
    def printToLog(self, s='', time=True, oarg='a'):
        if time:
            now = datetime.now()
            s = 'Job: '+self.name+' - '+now.strftime("%Y-%m-%d, %H:%M")+' :: '+s
        with open(self.logfile, oarg) as f:
            f.write(s+'\n')
            self.nline+=1

    # Return the tail of the logfile.
    def tail(self, n = -1):
        if not shutil.which('tail'):
            return "Cannot reach tail on this system."
        if n == -1:
            n = self.nline
        ret = subprocess.run(['tail','-n',str(n),self.logfile],stdout=subprocess.PIPE).stdout.decode('utf-8')
        return ret
        
