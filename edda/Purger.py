import os
import time
from datetime import datetime, timedelta
import random
import shutil
import subprocess

### Purges old directories in target based on their 
### time stamp, between tMin (default forever) and tMax 
### (default 14 days ago) with probability p (default 1).

class Purger:
    def __init__(self, name, logger, target, tMin = 1e4, tMax = 14, p = 1):
        self.name = name
        self.target = target
        self.tMin = tMin
        self.tMax = tMax
        self.p = p
        self.log = logger
        self.log.printToLog('New purging job: '+self.name)


    # Disk usage. Return summary string and percentage used.
    def diskUsage(self):
        multi = 1024**3
        du = shutil.disk_usage(self.target)
        tot = str(round(du[0]/multi))
        us = str(round(du[1]/multi))
        fre = str(round(du[2]/multi))
        p = round(100*du[1]/du[0])
        ret = 'Disk usage: Total='+tot+'G, Used='+us+'G ('+str(p)+'%), Free='+fre+'G'
        return ret, p

    # Do the actual purging.
    def purge(self):
        # Consistency check
        if not os.path.exists(self.target):
            self.log.printToLog('Purger could not find location: '+self.target)
            raise OSError('Purger could not find location')
        dirs = os.listdir(self.target)
        self.log.printToLog('Before purging:')
        st = ''
        for d in dirs:
            st +=d+"; " 
        self.log.printToLog(st)
        disk, p = self.diskUsage()
        self.log.printToLog(disk)
        ts = []
        dds = []
        for d in dirs:
            if d == 'latest':
                continue
            try: 
                ts.append(datetime.strptime(d,"%Y-%m-%d_%H:%M"))
                dds.append(d)
            except:
                self.log.printToLog("Bad directory: "+d)
        now = datetime.now()
        for t,d in zip(ts,dds):
            oldestDelta = timedelta(hours = 24 * self.tMin)
            delta = timedelta(hours = 24 * self.tMax)
            if now - t > oldestDelta:
                pass
            elif now - t > delta:
                if random.random() < self.p:
                    self.log.printToLog("Deleting: "+d)
                    args = ['rm', '-rf', self.target + '/' + d]
                    subprocess.run(args, stdout=subprocess.PIPE)
                else:
                    self.log.printToLog("Randomly skipping deletion of: "+d)
            else:
                pass
        dirs = os.listdir(self.target)
        self.log.printToLog('After purging:')
        st = ''
        for d in dirs:
            st +=d+"; " 
        self.log.printToLog(st)
        disk, p = self.diskUsage()
        self.log.printToLog(disk)
        self.log.printToLog('Done purging.')
