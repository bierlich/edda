# -*- coding: utf-8 -*-
import subprocess
import time

# Take a list of system call jobs and run them multi-
# threaded with subprocess.Popen().

class MultiThread:
    def __init__(self, jobs = [], nt = 1):
        # Number of threads to be used.
        self.nThreads = nt
        # The job queue.
        self.jobs = jobs

    def run(self):
        # Finished jobs.
        finished = []
        # Don't enable more threads than we have jobs in queue.
        nThreads = min(self.nThreads, len(self.jobs))
        # Start a job on all threads
        procs = [subprocess.Popen(self.jobs[i]) for i in range(self.nThreads)]

        # Start new jobs when old ones finishes
        iStart = len(procs)
        while len(procs):
            for i in range(len(procs)):
                if procs[i].poll() is not None:
                    finished.append(procs.pop(i))
                    if len(finished) + len(procs) < len(self.jobs):
                        procs.append(subprocess.Popen(self.jobs[iStart]))
                        iStart += 1
                    else:
                        break
            time.sleep(2)
        # Return these for logging purposes.
        return finished, self.jobs
