# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
import os
import subprocess
import time
import shutil
from .MultiThread import MultiThread

# Defines a backup job run with rsync and tar.
class Backup():
    def __init__(self, name, logger, method='diff', sources = [], target = '', stashd = 'stash/', excludes = [], nobackup = []):
        if not shutil.which('rsync'):
            raise Exception('rsync must be installed!')
        if not shutil.which('tar'):
            raise Exception('tar must be installed!')
        self.name = name
        self.log = logger
        self.log.printToLog('New backup job '+self.name+'.')
        self.sources = sources
        self.target = target
        self.stashdir = stashd
        self.excludes = excludes
        self.nobackup = nobackup
        self.hasErrors = False

        # Set the rsync args according to method.
        self.method = method
        self.rargs = ['rsync']
        if self.method == 'diff':
            self.rargs += ['-a', '--delete']
            self.target+='/latest/'
        elif self.method == 'snap':
            self.symlink = self.target+'/latest'
            self.rargs += ['-aPh', '--link-dest='+self.symlink+'/']
            now = datetime.now()
            s = now.strftime("%Y-%m-%d_%H:%M")
            self.target += '/'+s+'/'
        else:
            self.log.printToLog('Method '+self.method+' unavailable.')
            self.hasErrors = True
            raise Exception('Unavailable method.')

    # End the backup.
    def finish(self, s):
        self.log.printToLog(s)

    # Check if a backup source exists on local fs.
    def sourceExists(self, source):
        if not os.path.exists(source):
            self.hasErrors = True
            self.log.printToLog('Source directory '+source+' not found on local FS.')
            raise OSError('Source '+source+' not found!')
    
    # Check if a backup source exists remotely. If it does,
    # return the directories as by-product.
    def sourceExistsRemote(self, source):
        # Split source in remote server part and
        # path. Bail if no ":" in source.
        so = source.split(':')
        if len(so) != 2:
            self.hasErrors = True
            self.log.printToLog('Source '+source+' does not look like a remote directory.')
            raise OSError('Source '+source+' does not look like a remote directory!')
        arg = ['ssh', so[0], 'ls', so[1]]
        proc = subprocess.run(arg, stdout=subprocess.PIPE)
        if proc.returncode != 0:
            self.hasErrors = True
            self.log.printToLog('Source '+source+' not found remotely.')
            raise OSError('Source '+source+' not found remotely!')
        ret = proc.stdout.decode('utf-8')
        return ret.rstrip().split('\n')

    # Check if the target exists on local fs.
    def targetExists(self, make = False):
        if not os.path.exists(self.target):
            if make:
                subprocess.run(['mkdir','-p',self.target])
            else:
                self.hasErrors = True
                self.log.printToLog('Target directory '+self.target+' not found!')
                raise OSError('Target '+self.target+' not found!')

    # Check if the stash exists on local fs.
    def stashExists(self):
        if not os.path.exists(self.stashdir):
            self.hasErrors = True
            self.log.printToLog('Stash directory '+self.stashdir+' not found!')
            raise OSError('Stash '+self.stashdir+' not found!')
    
    # Run the rsync process.
    def rsync(self, source, target):
        # Logging and set up job.
        self.log.printToLog('Backing up '+source+' to '+target+'.')
        args = [a for a in self.rargs]
        for nb in self.nobackup:
            args.append('--exclude')
            args.append(nb)
        args+=[source, target]
        # Run rsync, catch the exception in calling function.
        try:
            proc = subprocess.run(args, stdout=subprocess.PIPE)
            self.log.printToLog('rsync finished with status: '+str(proc.returncode))
        except Exception as e:
            self.hasErrors = True
            self.log.printToLog('An error occured: '+str(e))
    
    # Perform the chosen backup of all sources.
    def backup(self):
        # Logging and sanity check.
        self.log.printToLog('Starting backup stage.')
        self.log.printToLog('Backup method is: '+self.method+'.')
        self.targetExists(True)

        # Go over source directories.
        for s in self.sources:
            # Test that we can access the source, get the source
            # directories, continue on errors.
            try:
                self.sourceExists(s)
                dirs = os.listdir(s)
            except OSError as err:
                # Try remote source
                self.hasErrors = False
                try:
                    dirs = self.sourceExistsRemote(s)
                except OSError as err2:
                    print(err)
                    print(err2)
                    continue
            # Set up the directories to back up.
            dirs = [d for d in dirs if d not in self.excludes]
            # Run rsync.
            for d in dirs:
                self.rsync(s+'/'+d, self.target)

        if self.method == 'snap':
            if os.path.exists(self.symlink):
                os.unlink(self.symlink)
            subprocess.run(['ln','-s',self.target,self.symlink])
        # Logging and done.
        self.log.printToLog('Finished backup stage.')
        if self.hasErrors:
            raise Exception('Unspecified errors. Check the logfile.')

    # Stash the current backup target in bzipped tar.
    def stash(self, stamp, nThreads):
        # Start with some consistency checks and logging.
        self.log.printToLog('Start stashing with stamp: \"'+stamp+'\".' )
        self.stashExists()
        
        # The stamped stash directory.
        stampdir = self.stashdir+'/'+stamp
        # Bail out if we were to overwrite an old backup.
        if os.path.exists(stampdir):
            self.hasError = True
            self.log.printToLog('ERROR: File stash with stamp: \"'+stamp+'\" already exists!')
            raise Exception('Stash error. Check the logfile.')
        else:
            subprocess.run(['mkdir',stampdir])
        
        # Set up the compression jobs.
        dirs = os.listdir(self.target)
        jobs = []
        for d in dirs:
            if self.stashdir in d:
                continue
            args = ['tar','cjSf',stampdir+'/'+d+'.tar.bz2','-C',self.target,d]
            jobs.append(args)
        
        # Run compression jobs multithreaded.
        m = MultiThread(jobs, nThreads)
        finished, jobs = m.run()
        
        # Finish logging and done.
        for p,j in zip(finished,jobs):
            s = 'Compression: \"'
            for a in j:
                s+=a+' '
            s = s[:-1]
            s+='\" returned: '+str(p.returncode)+'.'
            self.log.printToLog(s,False)
        self.log.printToLog('Done stashing.')


