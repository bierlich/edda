# -*- coding: utf-8 -*-
import os
import json
from stat import S_IRUSR
import getpass
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import subprocess

# Allows alert emails from an existing smtp server.
class Alert():
    def __init__(self, name, logger, config='config'):
        self.logger = logger
        self.logger.printToLog('Initializing Alerter.')
        if not os.path.exists(config):
            self.configure(config)
        with open(config, 'r') as f:
            self.settings = json.load(f)
        self.name = name

    def configure(self, config):
        self.logger.printToLog('Configuring SMTP, saving to '+config)
        print('We will now configure your email settings.')
        print('The settings will be saved to '+config+'.')
        settings = {}
        settings['fromaddr'] = input('The "From" address: ')
        settings['toaddr'] = input('The "To" address: ')
        settings['smtp'] = input('Your SMTP server: ')
        settings['port'] = int(input('Port: '))
        settings['login'] = input('Login: ')
        settings['password'] = getpass.getpass('Password: ')
        with open(config,'w+') as f:
            json.dump(settings, f)
        os.chmod(config, S_IRUSR)

    def send(self,body):
        self.logger.printToLog('Sending status mail to: '+self.settings['toaddr'])
        s = smtplib.SMTP(host = self.settings['smtp'], port=self.settings['port'])
        s.starttls()
        s.login(self.settings['login'],self.settings['password'])
        msg = MIMEMultipart()
        msg['From'] = self.settings['fromaddr']
        msg['To'] = self.settings['toaddr']
        msg['Subject'] = 'EDDA report from "'+self.name+'"'
        body+='The log from this session (logfile: '+self.logger.logfile+') is pasted below.\n\n\n\n'
        ret = self.logger.tail()
        body+=ret
        msg.attach(MIMEText(body,'plain'))
        text = msg.as_string()
        s.sendmail(self.settings['fromaddr'],self.settings['toaddr'],text)

    def sendCompletedNoErrors(self):
        body = 'Hi!\n\nThe backup job '+self.name+' finished without errors!\n\n\n'
        self.send(body)

    def sendCompletedWithErrors(self):
        body = 'Hi!\n\nWarning: The backup job '+self.name+' finished with errors!\n\n\n'
        self.send(body)
