# -*- coding: utf-8 -*-

from setuptools import find_packages, setup


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='edda',
    packages=find_packages(include=['edda']),
    version='0.1.0',
    description='Simple Python3 backup tool based on rsync',
    author='Christian Bierlich',
    author_email='<christian.bierlich@thep.lu.se>',
    license=license,
    long_description=readme,
    install_requires=['setuptools','datetime'],
)
